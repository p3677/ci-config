#!/usr/bin/perl

use strict;
use warnings;


my ($thea_exe, $dirname, $signKey, $previousTags) = @ARGV;


$thea_exe = "echo thea" if not defined $thea_exe;
$dirname = "center-Windows_v0.0.4" if not defined $dirname;
#~ my $scp = "echo scp";
my $scp = "scp";


my ($component, $currentTag) = split /_/, $dirname;

print $component."\n";
print $currentTag."\n";

my $server = "files\@51.75.92.201";

my $thea_sign = "";
$thea_sign = "--sign $signKey" if $signKey ne "";

sub createPatch {
	my ($tag) = @_;
	
	my $patchDir = "${component}_${dirname}";
	my $filename = "${patchDir}.thea";
	
	# download
	print "$scp ${server}:/opt/ows/files/$filename .\n";
	print `$scp ${server}:/opt/ows/files/$filename .` or goto ERROR;
	
	# extract
	print "$thea_exe extract $filename $patchDir\n";
	print `$thea_exe extract $filename $patchDir` or goto ERROR;
	
	# create patch
	my $patchfile = "${component}_${tag}_${currentTag}.thea";
	print "$thea_exe archive --compression best $thea_sign --old $patchDir $dirname $patchfile\n";
	print `$thea_exe archive --compression best $thea_sign --old $patchDir $dirname $patchfile` or goto ERROR;
	
	# create digest
	print "$thea_exe digest $patchfile > $patchfile.blake2b\n";
	print `$thea_exe digest $patchfile > $patchfile.blake2b` or goto ERROR;
	
	# upload
	print "$scp $patchfile ${server}:/opt/ows/files\n";
	print `$scp $patchfile ${server}:/opt/ows/files` or goto ERROR;
	
	# upload digest
	print "$scp $patchfile.blake2b ${server}:/opt/ows/files\n";
	print `$scp $patchfile.blake2b ${server}:/opt/ows/files` or goto ERROR;
	
	print "\n";
	return;
	
	ERROR:
	print "ERROR creating patch for $tag:\n";
	print $!;
}

if( not defined $previousTags or $previousTags eq ""){
	my $lastTag;
	my $last;
	my $tags = `git for-each-ref --sort=-creatordate --format '%(refname:lstrip=2)' refs/tags` or die $!;
	
	foreach( split /\n/, $tags ){
		next if( $_ eq $currentTag );
		
		if( /^v\d+\.\d+\.\d+$/ ){
			$lastTag = $_;
			last;
		}elsif( not defined $last or $last eq ''){
			$last = $_;
		}
	}
	
	if( defined $last ){
		print "last $last\n";
		createPatch($last);
	}
	
	if( defined $lastTag ){
		print "lastTag $lastTag\n";
		createPatch($lastTag);
	}
}elsif( $previousTags ne "none"){
	foreach( split /;/, $previousTags ){
		print "$_\n";
	}
}


print "$thea_exe archive --compression best $thea_sign $dirname $dirname.thea\n";
print `$thea_exe archive --compression best $thea_sign $dirname $dirname.thea` or die $!;

print "$scp $dirname.thea ${server}:/opt/ows/files\n";
print `$scp $dirname.thea ${server}:/opt/ows/files` or die $!;

# create digest
print "$thea_exe digest $dirname.thea > $dirname.thea.blake2b\n";
print `$thea_exe digest $dirname.thea > $dirname.thea.blake2b` or goto ERROR;

# upload digest
print "$scp $dirname.thea.blake2b ${server}:/opt/ows/files\n";
print `$scp $dirname.thea.blake2b ${server}:/opt/ows/files` or goto ERROR;
